# deltager2webscorer
a little python script that converts start lists from deltager.no into a webscorer.com accepted csv format

## howto
- download [doTheMagic.py](https://github.com/klevstul/deltager2webscorer/blob/master/src/doTheMagic.py) to a folder of your choice on your computer.
- add the deltager.no start list to this same folder
- rename the start list from deltager.no so its name becomes `deltager.xls`
- run the python script using the command `python doTheMagic.py`

## prerequisite
you need to have [python 2.7](https://www.python.org) installed on your computer. then the python package [xlrd](https://pypi.python.org/pypi/xlrd) is needed. for xlrd check the [windows installation notes](https://github.com/klevstul/deltager2webscorer/issues/1)

## java version
a java solution, doing the same job, is located at [aoksenholt/deltager-webscorer](https://github.com/aoksenholt/deltager-webscorer).
