# -*- coding: iso-8859-15 -*-
# PEP 263 -- Defining Python Source Code Encodings (https://www.python.org/dev/peps/pep-0263/)
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# author:       frode at thisworld dot is (thisworld.is)
# started:      17.07.13
# disclaimer:   haven't spent much time on writing good code.
#               threw together some lines. they do the job.
#               that is it. that is all.
# info:
#   deltager header:
#       Etternavn | Fornavn | E-post adresse | Mobil | Klubb/bedrift/sted | Fødselsår - 4 siffer | Klasse
#   webscorer complete header (https://www.webscorer.com/resources/templatestart):
#       Name | First name | Last name | Team name | Team name 2 | Gender | Age | Distance | Category | Wave | Bib | Start time | Predicted time | Handicap | Chip ID | Chip ID2 | Info 1 | Info 2 | Info 3 | Info 4 | Info 5 | Email
#   our webscorer header:
#       Last name | First name | Email | Info 1 | Team name | Info 2 | Category | Bib* | Gender* | Age*    * - values determined by logic
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
import xlrd
import csv
import sys
import datetime

reload(sys)
sys.setdefaultencoding('utf-8')

def csv_from_excel():

    wb = xlrd.open_workbook('deltager.xls')
    sh = wb.sheet_by_name('RadGridExport')
    csv_file = open('webscorer.csv', 'wb')
    wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)

    # life is a loop
    for rownum in xrange(sh.nrows):

        # apply some magic to the header row
        if rownum == 0:
            header = sh.row_values(rownum)
            cellnum = 0;

            for cell in sh.row_values(rownum):
                if cell.encode('utf-8').lower() == 'etternavn':
                    header[cellnum] = 'Last name'
                elif cell.encode('utf-8').lower() == 'fornavn':
                    header[cellnum] = 'First name'
                elif cell.encode('utf-8').lower() == 'e-post adresse':
                    header[cellnum] = 'Email'
                elif cell.encode('utf-8').lower() == 'mobil':
                    header[cellnum] = 'Info 1'
                elif cell.encode('utf-8').lower() == 'klubb/bedrift/sted':
                    header[cellnum] = 'Team name'
                elif cell.encode('utf-8').lower() == 'fødselsår - 4 siffer':
                    header[cellnum] = 'Info 2'
                elif cell.encode('utf-8').lower() == 'klasse':
                    header[cellnum] = 'Category'

                cellnum = cellnum + 1

            # add new header cells
            header.append('Bib')
            header.append('Gender')
            header.append('Age')

            # write header
            wr.writerow(header)

        # spread some lovely magic on the data rows
        else:
            data_row = sh.row_values(rownum)

            # set bib
            data_row.append(rownum)

            # set the correct gender
            if data_row[header.index('Category')].lower().startswith('m'):
                data_row.append('Male')
            elif data_row[header.index('Category')].lower().startswith('k'):
                data_row.append('Female')
            else:
                print 'WARNING: can not determine the gender. assumingly wrong category.\n' + str(data_row)

            # set age
            try:
                birth_year = int(data_row[header.index('Info 2')])
            except:
                birth_year = 0

            if birth_year < 1950 or birth_year > 2010:
                print 'WARNING: something smells fishy!\n' + str(data_row)
                # give everyone with wrong year the age of 100. sorry guys. you asked for it.
                birth_year = datetime.datetime.now().year - 100

            data_row.append(datetime.datetime.now().year - birth_year)

            # write data row
            wr.writerow(data_row)

    csv_file.close()

def main():
    print '*** magic is flowing through the universe (at least some bits are flowing through your computer) ***\n'
    csv_from_excel()

if '__main__' == __name__:
    main()
